const path = require("path");
const assets_dir = 'pub';
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
  entry: {core: "./src/js/core.js", polyfill: "./src/js/polyfill.js"},
  mode: process.env.NODE_ENV || 'development',
  output: {
    path: path.resolve(__dirname, assets_dir),
    filename: "./js/[name].js",
    publicPath: assets_dir
  },
  target: "node",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          },
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].css',
              context: './',
              outputPath: '/css',
              publicPath: '/css'
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader',
            options: {
              url: false,
              importLoaders: 1,
              modules: false
            }
          },
          //'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false
            }
          }
        ]
      },
      /*{
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              url: false,
              emitFile: false,
            }
          }
        ]
      }*/
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        extractComments: false,
        exclude: /\/node_modules/,
        test: /\.js(\?.*)?$/i,
        uglifyOptions: {
          output: {
            comments: false
          },
          warnings: false,
          compress: true,
          beautify: false,
          mangle: true,
          toplevel: true,
          nameCache: null,
          ie8: false,
          keep_fnames: false,
        },
        cache: true,
        parallel: 8,
        sourceMap: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  }
};