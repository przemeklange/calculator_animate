export const is_observer = () => {
  return ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) ? true : false;
};

export const each = (arr, fn) => {
  let i = 0, l = arr.length;
  for (i; i < l; i++) fn(i, arr[i]);
};

export const addCSS = (src, cfg = {id: false, cb: false}) => {
  if (cfg.id && document.body.getElementsByClassName(cfg.id)[0]) return cfg.cb();
  let el;

  el = document.createElement('link');
  cfg.id && el.classList.add(cfg.id);
  el.rel = 'stylesheet';
  el.href = src;

  document.head.appendChild(el);
  cfg.cb && el.addEventListener('load', cfg.cb, false);
};

export const addJS = (src, cfg = {id: false, cb: false}) => {
  if (cfg.id && document.body.getElementsByClassName(cfg.id)[0]) return cfg.cb();
  let el;

  el = document.createElement('script');
  cfg.id && el.classList.add(cfg.id);
  el.async = false;
  el.src = src;

  document.body.appendChild(el);
  cfg.cb && el.addEventListener('load', cfg.cb, false);
};

export const isRWD = (val) => {
  return window.innerWidth <= val ? true : false;
};

export const clearDom = (el) => {
  if (Array.isArray(el)) return clearAll(el);
  while (el.firstChild) {
    el.removeChild(el.firstChild);
  }
};

export const clearAll = (arr) => {
  each(arr, (key, val) => {
    clearDom(val);
  })
};

export const get_lang = () => {
  return window.navigator.language.toLowerCase().substring(0, 2);
};

export const render = (relEl, tpl) => {
  if (!relEl) return;
  let child;

  if (tpl.nodeName === 'TEMPLATE') {
    child = document.importNode(tpl.content, true);
  } else if (typeof tpl === 'string') {
    const range = document.createRange();
    range.selectNode(relEl);
    child = range.createContextualFragment(tpl);
  } else {
    child = tpl;
  }

  relEl.appendChild(child);
  return relEl.lastElementChild;
};

export const is_touch = () => {
  return 'ontouchstart' in document.documentElement;
};

export const fireEv = (el, name, cfg = {}) => {
  let ev = new CustomEvent(name, {detail: cfg});
  return {el: el, name: name, ev: ev};
};

export const del = (el) => {
  if (!el || !el.parentNode) return;
  return el.parentNode.removeChild(el);
};

export const uniq = (arr) => {
  return arr.filter((item, pos) => {
    return arr.indexOf(item) === pos;
  });
};