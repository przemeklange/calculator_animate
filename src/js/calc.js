const buttons = document.querySelectorAll('button');
const display = document.querySelector('.display');
const pyro = document.querySelector('.pyro')

buttons.forEach(function (button) {
    button.addEventListener('click', calculate);
});

function calculate(event) {
    const clickedButtonValue = event.target.value;

    if (clickedButtonValue === '=') {
        if (display.value !== '') {
            display.value = eval(display.value);
            if (display.value == '9') {
                pyro.innerHTML = "<div class='before'></div> <div class='after'></div>"
            }
        }
    } else if (clickedButtonValue === 'C') {
        display.value = '';
        pyro.innerHTML = '';
    } else {
        display.value += clickedButtonValue;
    }
}

class Calc {
    
}