const ProxyPolyfill = require('proxy-polyfill/src/proxy')(); // from NPM
//const ProxyPolyfill = require('./vendors/proxy')(); // local modified version

// ======== es6 promise polyfill =========
const Promise = require('es6-promise/auto');

// ======== fetch polyfill =========
import 'abortcontroller-polyfill/dist/abortcontroller-polyfill-only'
import {fetch as fetchPolyfill} from 'whatwg-fetch';

if (!'signal' in new Request('')) window.fetch = fetchPolyfill;

/*let controller = new AbortController();
fetch('core.php', {
  signal: controller.signal,
  credentials: 'include',
  mode: 'cors',
  method: 'post',
  /!*body: JSON.stringify({
    restore: '1'
  }),*!/
  //redirect: 'follow', // manual, *follow, error
  //referrer: 'no-referrer', // no-referrer, *client
  headers: {
    //'Content-Type': 'application/json'
    //'Content-Type': 'multipart/form-data'
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  body: 'restore=1'
}).then((response) => {
  if (response.status !== 200) controller.abort();
  return response.json();
}).then((response) => {
  return console.log(response);
}).catch((err) => {
  return console.log(err.name);
});*/

/*who() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(':-)');
    }, 200);
  });
}

what() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('lurks');
    }, 300);
  });
}

where() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('in the shadows');
    }, 500);
  });
}

static async pending() {
  return 'start pending...';
}

static async stopPending() {
  return 'stop pending after 1s';
}

async msg() {

  ccApi.pending().then(respone => console.log(respone));

  const [a, b, c, d] = await Promise.all([this.who(), this.what(), this.where(), ccApi.stopPending()]);
  console.log(`${ a } ${ b } ${ c }, ${ d }`);
}*/

window.Proxy_ = (...args) => {
  let proxy = new ProxyPolyfill(...args);
  return proxy;
};

(function (arr) {
  arr.forEach(function (item) {
    if (item.hasOwnProperty('prepend')) {
      return;
    }
    Object.defineProperty(item, 'prepend', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function prepend() {
        var argArr = Array.prototype.slice.call(arguments),
          docFrag = document.createDocumentFragment();

        argArr.forEach(function (argItem) {
          var isNode = argItem instanceof Node;
          docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
        });

        this.insertBefore(docFrag, this.firstChild);
      }
    });
  });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

if (typeof window.CustomEvent !== 'function') {
  window.CustomEvent = (event, params) => {
    params = params || {bubbles: false, cancelable: false, detail: undefined};
    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  };
  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
}

if (!('content' in document.createElement('template'))) {
  let qPlates = document.getElementsByTagName('template'), plateLen = qPlates.length, elPlate, qContent, contentLen,
    docContent;

  for (let x = 0; x < plateLen; ++x) {
    elPlate = qPlates[x];
    qContent = elPlate.childNodes;
    contentLen = qContent.length;
    docContent = document.createDocumentFragment();

    while (qContent[0]) {
      docContent.appendChild(qContent[0]);
    }
    elPlate.content = docContent;
  }
}

if (typeof Object.assign !== 'function') {
  Object.assign = function(target, varArgs) { // .length of function is 2
    'use strict';
    if (target == null) { // TypeError if undefined or null
      throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
      var nextSource = arguments[index];

      if (nextSource != null) { // Skip over if undefined or null
        for (var nextKey in nextSource) {
          // Avoid bugs when hasOwnProperty is shadowed
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  };
}

if (!(new DOMParser().parseFromString('', 'text/html'))) {
  //'use strict';
  let proto = DOMParser.prototype, nativeParse = proto.parseFromString;

  proto.parseFromString = function (markup, type) {
    if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {
      let doc = document.implementation.createHTMLDocument('');
      if (markup.toLowerCase().indexOf('<!doctype') > -1) {
        doc.documentElement.innerHTML = markup;
      }
      else {
        doc.body.innerHTML = markup;
      }
      return doc;
    } else {
      return nativeParse.apply(this, arguments);
    }
  };
}

if (!Object.entries) {
  Object.entries = (obj) => {
    let ownProps = Object.keys(obj),
      i = ownProps.length,
      resArray = new Array(i); // preallocate the Array
    while (i--)
      resArray[i] = [ownProps[i], obj[ownProps[i]]];
    return resArray;
  };
}

if (typeof Array.isArray === 'undefined') {
  Array.isArray = (obj) => {
    return Object.prototype.toString.call(obj) === '[object Array]';
  }
}

if (!Array.prototype.includes) {
  Array.prototype.includes = function (searchElement) {
    'use strict';
    var O = Object(this);
    var len = parseInt(O.length) || 0;
    if (len === 0) {
      return false;
    }
    var n = parseInt(arguments[1]) || 0;
    var k;
    if (n >= 0) {
      k = n;
    } else {
      k = len + n;
      if (k < 0) {
        k = 0;
      }
    }
    var currentElement;
    while (k < len) {
      currentElement = O[k];
      if (searchElement === currentElement ||
        (searchElement !== searchElement && currentElement !== currentElement)) {
        return true;
      }
      k++;
    }
    return false;
  };
}

/*if (!String.prototype.includes) {
  String.prototype.includes = (search, start) => {
    //'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }

    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}*/

Number.isInteger = Number.isInteger || function (value) {
  return typeof value === 'number' &&
    isFinite(value) &&
    Math.floor(value) === value;
};

/*
if (!('prepend' in Element.prototype) || !('append' in Element.prototype)) {

  HTMLElement = typeof(HTMLElement) !== 'undefined' ? HTMLElement : Element;

  HTMLElement.prototype.prepend = function (element) {
    if (this.firstChild) {
      return this.insertBefore(element, this.firstChild);
    } else {
      return this.appendChild(element);
    }
  };

  HTMLElement.prototype.append = function (element) {
    if (this.firstChild) {
      return this.insertBefore(element, this.firstChild);
    } else {
      return this.appendChild(element);
    }
  };
}
*/

(function (window, document) {
  'use strict';

  // Exits early if all IntersectionObserver and IntersectionObserverEntry
  // features are natively supported.
  if ('IntersectionObserver' in window &&
    'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {

    // Minimal polyfill for Edge 15's lack of `isIntersecting`
    // See: https://github.com/w3c/IntersectionObserver/issues/211
    if (!('isIntersecting' in window.IntersectionObserverEntry.prototype)) {
      Object.defineProperty(window.IntersectionObserverEntry.prototype,
        'isIntersecting', {
          get: function () {
            return this.intersectionRatio > 0;
          }
        });
    }
    return;
  }

  let registry = [];

  function IntersectionObserverEntry(entry) {
    this.time = entry.time;
    this.target = entry.target;
    this.rootBounds = entry.rootBounds;
    this.boundingClientRect = entry.boundingClientRect;
    this.intersectionRect = entry.intersectionRect || getEmptyRect();
    this.isIntersecting = !!entry.intersectionRect;

    // Calculates the intersection ratio.
    let targetRect = this.boundingClientRect, targetArea = targetRect.width * targetRect.height,
      intersectionRect = this.intersectionRect, intersectionArea = intersectionRect.width * intersectionRect.height;

    // Sets intersection ratio.
    if (targetArea) {
      this.intersectionRatio = intersectionArea / targetArea;
    } else {
      // If area is zero and is intersecting, sets to 1, otherwise to 0
      this.intersectionRatio = this.isIntersecting ? 1 : 0;
    }
  }

  function IntersectionObserver(callback, opt_options) {

    let options = opt_options || {};

    if (typeof callback !== 'function') {
      throw new Error('callback must be a function');
    }

    if (options.root && options.root.nodeType !== 1) {
      throw new Error('root must be an Element');
    }

    // Binds and throttles `this._checkForIntersections`.
    this._checkForIntersections = throttle(
      this._checkForIntersections.bind(this), this.THROTTLE_TIMEOUT);

    // Private properties.
    this._callback = callback;
    this._observationTargets = [];
    this._queuedEntries = [];
    this._rootMarginValues = this._parseRootMargin(options.rootMargin);

    // Public properties.
    this.thresholds = this._initThresholds(options.threshold);
    this.root = options.root || null;
    this.rootMargin = this._rootMarginValues.map(function (margin) {
      return margin.value + margin.unit;
    }).join(' ');
  }

  IntersectionObserver.prototype.THROTTLE_TIMEOUT = 100;
  IntersectionObserver.prototype.POLL_INTERVAL = null;
  IntersectionObserver.prototype.USE_MUTATION_OBSERVER = true;
  IntersectionObserver.prototype.observe = function (target) {
    let isTargetAlreadyObserved = this._observationTargets.some(function (item) {
      return item.element === target;
    });

    if (isTargetAlreadyObserved) {
      return;
    }

    if (!(target && target.nodeType === 1)) {
      throw new Error('target must be an Element');
    }

    this._registerInstance();
    this._observationTargets.push({element: target, entry: null});
    this._monitorIntersections();
    this._checkForIntersections();
  };

  IntersectionObserver.prototype.unobserve = function (target) {
    this._observationTargets =
      this._observationTargets.filter(function (item) {

        return item.element !== target;
      });
    if (!this._observationTargets.length) {
      this._unmonitorIntersections();
      this._unregisterInstance();
    }
  };

  IntersectionObserver.prototype.disconnect = function () {
    this._observationTargets = [];
    this._unmonitorIntersections();
    this._unregisterInstance();
  };

  IntersectionObserver.prototype.takeRecords = function () {
    let records = this._queuedEntries.slice();
    this._queuedEntries = [];
    return records;
  };

  IntersectionObserver.prototype._initThresholds = function (opt_threshold) {
    let threshold = opt_threshold || [0];
    if (!Array.isArray(threshold)) threshold = [threshold];

    return threshold.sort().filter(function (t, i, a) {
      if (typeof t !== 'number' || isNaN(t) || t < 0 || t > 1) {
        throw new Error('threshold must be a number between 0 and 1 inclusively');
      }
      return t !== a[i - 1];
    });
  };

  IntersectionObserver.prototype._parseRootMargin = function (opt_rootMargin) {
    let marginString = opt_rootMargin || '0px', margins = marginString.split(/\s+/).map(function (margin) {
      let parts = /^(-?\d*\.?\d+)(px|%)$/.exec(margin);
      if (!parts) {
        throw new Error('rootMargin must be specified in pixels or percent');
      }
      return {value: parseFloat(parts[1]), unit: parts[2]};
    });

    // Handles shorthand.
    margins[1] = margins[1] || margins[0];
    margins[2] = margins[2] || margins[0];
    margins[3] = margins[3] || margins[1];

    return margins;
  };

  IntersectionObserver.prototype._monitorIntersections = function () {
    if (!this._monitoringIntersections) {
      this._monitoringIntersections = true;

      // If a poll interval is set, use polling instead of listening to
      // resize and scroll events or DOM mutations.
      if (this.POLL_INTERVAL) {
        this._monitoringInterval = setInterval(
          this._checkForIntersections, this.POLL_INTERVAL);
      }
      else {
        addEvent(window, 'resize', this._checkForIntersections, true);
        addEvent(document, 'scroll', this._checkForIntersections, true);

        if (this.USE_MUTATION_OBSERVER && 'MutationObserver' in window) {
          this._domObserver = new MutationObserver(this._checkForIntersections);
          this._domObserver.observe(document, {
            attributes: true,
            childList: true,
            characterData: true,
            subtree: true
          });
        }
      }
    }
  };

  IntersectionObserver.prototype._unmonitorIntersections = function () {
    if (this._monitoringIntersections) {
      this._monitoringIntersections = false;

      clearInterval(this._monitoringInterval);
      this._monitoringInterval = null;

      removeEvent(window, 'resize', this._checkForIntersections, true);
      removeEvent(document, 'scroll', this._checkForIntersections, true);

      if (this._domObserver) {
        this._domObserver.disconnect();
        this._domObserver = null;
      }
    }
  };

  IntersectionObserver.prototype._checkForIntersections = function () {
    let rootIsInDom = this._rootIsInDom(), rootRect = rootIsInDom ? this._getRootRect() : getEmptyRect();

    this._observationTargets.forEach(function (item) {
      let target = item.element;
      let targetRect = getBoundingClientRect(target);
      let rootContainsTarget = this._rootContainsTarget(target);
      let oldEntry = item.entry;
      let intersectionRect = rootIsInDom && rootContainsTarget &&
        this._computeTargetAndRootIntersection(target, rootRect);

      let newEntry = item.entry = new IntersectionObserverEntry({
        time: now(),
        target: target,
        boundingClientRect: targetRect,
        rootBounds: rootRect,
        intersectionRect: intersectionRect
      });

      if (!oldEntry) {
        this._queuedEntries.push(newEntry);
      } else if (rootIsInDom && rootContainsTarget) {
        // If the new entry intersection ratio has crossed any of the
        // thresholds, add a new entry.
        if (this._hasCrossedThreshold(oldEntry, newEntry)) {
          this._queuedEntries.push(newEntry);
        }
      } else {
        // If the root is not in the DOM or target is not contained within
        // root but the previous entry for this target had an intersection,
        // add a new record indicating removal.
        if (oldEntry && oldEntry.isIntersecting) {
          this._queuedEntries.push(newEntry);
        }
      }
    }, this);

    if (this._queuedEntries.length) {
      this._callback(this.takeRecords(), this);
    }
  };

  IntersectionObserver.prototype._computeTargetAndRootIntersection =
    function (target, rootRect) {

      // If the element isn't displayed, an intersection can't happen.
      if (window.getComputedStyle(target).display === 'none') return;

      let targetRect = getBoundingClientRect(target);
      let intersectionRect = targetRect;
      let parent = getParentNode(target);
      let atRoot = false;

      while (!atRoot) {
        let parentRect = null;
        let parentComputedStyle = parent.nodeType === 1 ?
          window.getComputedStyle(parent) : {};

        // If the parent isn't displayed, an intersection can't happen.
        if (parentComputedStyle.display === 'none') return;

        if (parent === this.root || parent === document) {
          atRoot = true;
          parentRect = rootRect;
        } else {
          // If the element has a non-visible overflow, and it's not the <body>
          // or <html> element, update the intersection rect.
          // Note: <body> and <html> cannot be clipped to a rect that's not also
          // the document rect, so no need to compute a new intersection.
          if (parent !== document.body &&
            parent !== document.documentElement &&
            parentComputedStyle.overflow !== 'visible') {
            parentRect = getBoundingClientRect(parent);
          }
        }

        // If either of the above conditionals set a new parentRect,
        // calculate new intersection data.
        if (parentRect) {
          intersectionRect = computeRectIntersection(parentRect, intersectionRect);

          if (!intersectionRect) break;
        }
        parent = getParentNode(parent);
      }
      return intersectionRect;
    };

  IntersectionObserver.prototype._getRootRect = function () {
    var rootRect;
    if (this.root) {
      rootRect = getBoundingClientRect(this.root);
    } else {
      // Use <html>/<body> instead of window since scroll bars affect size.
      let html = document.documentElement;
      let body = document.body;
      rootRect = {
        top: 0,
        left: 0,
        right: html.clientWidth || body.clientWidth,
        width: html.clientWidth || body.clientWidth,
        bottom: html.clientHeight || body.clientHeight,
        height: html.clientHeight || body.clientHeight
      };
    }
    return this._expandRectByRootMargin(rootRect);
  };

  IntersectionObserver.prototype._expandRectByRootMargin = function (rect) {
    let margins = this._rootMarginValues.map(function (margin, i) {
      return margin.unit === 'px' ? margin.value :
        margin.value * (i % 2 ? rect.width : rect.height) / 100;
    });
    let newRect = {
      top: rect.top - margins[0],
      right: rect.right + margins[1],
      bottom: rect.bottom + margins[2],
      left: rect.left - margins[3]
    };
    newRect.width = newRect.right - newRect.left;
    newRect.height = newRect.bottom - newRect.top;

    return newRect;
  };

  IntersectionObserver.prototype._hasCrossedThreshold =
    function (oldEntry, newEntry) {

      // To make comparing easier, an entry that has a ratio of 0
      // but does not actually intersect is given a value of -1
      var oldRatio = oldEntry && oldEntry.isIntersecting ?
        oldEntry.intersectionRatio || 0 : -1;
      var newRatio = newEntry.isIntersecting ?
        newEntry.intersectionRatio || 0 : -1;

      // Ignore unchanged ratios
      if (oldRatio === newRatio) return;

      for (var i = 0; i < this.thresholds.length; i++) {
        var threshold = this.thresholds[i];

        // Return true if an entry matches a threshold or if the new ratio
        // and the old ratio are on the opposite sides of a threshold.
        if (threshold === oldRatio || threshold === newRatio ||
          threshold < oldRatio !== threshold < newRatio) {
          return true;
        }
      }
    };

  IntersectionObserver.prototype._rootIsInDom = function () {
    return !this.root || containsDeep(document, this.root);
  };

  IntersectionObserver.prototype._rootContainsTarget = function (target) {
    return containsDeep(this.root || document, target);
  };

  IntersectionObserver.prototype._registerInstance = function () {
    if (registry.indexOf(this) < 0) {
      registry.push(this);
    }
  };

  IntersectionObserver.prototype._unregisterInstance = function () {
    var index = registry.indexOf(this);
    if (index !== -1) registry.splice(index, 1);
  };

  function now() {
    return window.performance && performance.now && performance.now();
  }

  function throttle(fn, timeout) {
    var timer = null;
    return function () {
      if (!timer) {
        timer = setTimeout(function () {
          fn();
          timer = null;
        }, timeout);
      }
    };
  }

  function addEvent(node, event, fn, opt_useCapture) {
    if (typeof node.addEventListener === 'function') {
      node.addEventListener(event, fn, opt_useCapture || false);
    }
    else if (typeof node.attachEvent === 'function') {
      node.attachEvent('on' + event, fn);
    }
  }

  function removeEvent(node, event, fn, opt_useCapture) {
    if (typeof node.removeEventListener === 'function') {
      node.removeEventListener(event, fn, opt_useCapture || false);
    }
    else if (typeof node.detatchEvent === 'function') {
      node.detatchEvent('on' + event, fn);
    }
  }

  function computeRectIntersection(rect1, rect2) {
    let top = Math.max(rect1.top, rect2.top);
    let bottom = Math.min(rect1.bottom, rect2.bottom);
    let left = Math.max(rect1.left, rect2.left);
    let right = Math.min(rect1.right, rect2.right);
    let width = right - left;
    let height = bottom - top;

    return (width >= 0 && height >= 0) && {
      top: top,
      bottom: bottom,
      left: left,
      right: right,
      width: width,
      height: height
    };
  }

  function getBoundingClientRect(el) {
    let rect;

    try {
      rect = el.getBoundingClientRect();
    } catch (err) {
      // Ignore Windows 7 IE11 "Unspecified error"
      // https://github.com/w3c/IntersectionObserver/pull/205
    }

    if (!rect) return getEmptyRect();

    // Older IE
    if (!(rect.width && rect.height)) {
      rect = {
        top: rect.top,
        right: rect.right,
        bottom: rect.bottom,
        left: rect.left,
        width: rect.right - rect.left,
        height: rect.bottom - rect.top
      };
    }
    return rect;
  }

  function getEmptyRect() {
    return {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      width: 0,
      height: 0
    };
  }

  function containsDeep(parent, child) {
    let node = child;
    while (node) {
      if (node === parent) return true;

      node = getParentNode(node);
    }
    return false;
  }

  function getParentNode(node) {
    let parent = node.parentNode;

    if (parent && parent.nodeType === 11 && parent.host) {
      // If the parent is a shadow root, return the host element.
      return parent.host;
    }
    return parent;
  }


  // Exposes the constructors globally.
  window.IntersectionObserver = IntersectionObserver;
  window.IntersectionObserverEntry = IntersectionObserverEntry;

}(window, document));